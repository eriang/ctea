/*
 * ctea_app_a.c
 * 
 * App A for Code Test by Erik Anggard (ctea)
 *
 * Overview:
 * - Reads AP data from a json file
 * - Parses it and looks for any changes
 * - Send any changes to App B
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <json-c/json.h>
#include <uthash.h>

#include "ctea.h"
#include "ctea_watch.h"

/* 
 * ap_item_t
 *
 * Struct for storing AP data items in a hash (hashed on SSID).
 */
typedef struct ap_item {
   access_point_data_t  api_ap;
   int                  api_found;
   UT_hash_handle       api_hh;
} ap_item_t;


/*
 * Globals
 */
ap_item_t *aps = NULL; // Head of hash
int fifo_fd = -1; // FIFO (named pipe) FD used for IPC App A->B
int fdbg = 0; // debug flag
int ftst = 0; // test flag


/*
 * inform_access_point_update
 *
 * Inform App B about AP update.
 */
int
inform_access_point_update(access_point_data_t *old, access_point_data_t *new)
{
   access_point_data_t msg[2];

   memcpy(&msg[0], old, sizeof(*old));
   memcpy(&msg[1], new, sizeof(*new));
   write(fifo_fd, msg, sizeof(msg));
   
   return 0;
}

/*
 * handle_access_point
 *
 * Handle AP data:
 * - Check if we already have data for AP with same SSID, in that case
 *   update it if needed and inform app B of changes.
 * - if first is true (ie. first read of file) we skipp informing B.
 * - api_found field is used to mark the item so it will not be removed
 *   when later purging removed entries.
 */
int
handle_access_point(const char *ssid, int32_t snr, int32_t channel, int first)
{
   ap_item_t *api;
   access_point_data_t old;
   unsigned keylen = (unsigned)uthash_strlen(ssid);
   
   /* Look for given SSID in hash. */
   HASH_FIND(api_hh, aps, ssid, keylen, api);

   if (api != NULL) {
      /* Found, check if update is needed. */
      api->api_found = 1;
      if (api->api_ap.ap_snr != snr || api->api_ap.ap_channel != channel) {
         old = api->api_ap;
         api->api_ap.ap_snr = snr;
         api->api_ap.ap_channel = channel;
         inform_access_point_update(&old, &api->api_ap);
      }
   } else {
      /* New entry. */
      api = (ap_item_t *)calloc(1, sizeof(*api));
      strncpy((char *)&api->api_ap.ap_ssid, ssid, sizeof(api->api_ap.ap_ssid));
      api->api_ap.ap_ssid[SSID_MAX_LEN] = '\0';
      api->api_ap.ap_snr = snr;
      api->api_ap.ap_channel = channel;
      api->api_found = 1;
      HASH_ADD(api_hh, aps, api_ap.ap_ssid, keylen, api);
      if (!first) {
         memset(&old, 0, sizeof(old));
         inform_access_point_update(&old, &api->api_ap);
      }
   }

   return 0;
}

/*
 * parse_access_points
 *
 * Parse JSON data of access_points part
 */
int
parse_access_points(json_object *root, int first)
{
   json_object *access_points = json_object_object_get(root, "access_points");
   if (json_object_get_type(access_points) != json_type_array) {
      fprintf(stderr, "json error: access_points not of array type\n");
      return 1;      
   }

   int n = json_object_array_length(access_points);
   for (int i=0; i<n; i++) {
      json_object *ap, *ssid, *snr, *channel;
      const char *ssid_s;
      int32_t snr_n;
      int32_t channel_n;
      ap = json_object_array_get_idx(access_points, i);
      if ((ssid = json_object_object_get(ap, "ssid")) == NULL ||
         json_object_get_type(ssid) != json_type_string) {
         fprintf(stderr, 
            "json error: ssid missing or invalid in entry %d\n", i);
         continue;
      }
      if ((snr = json_object_object_get(ap, "snr")) == NULL ||
         json_object_get_type(snr) != json_type_int) {
         fprintf(stderr, 
            "json error: snr missing or invalid in entry %d\n", i);
         continue;
      }
      if ((channel = json_object_object_get(ap, "channel")) == NULL ||
         json_object_get_type(snr) != json_type_int) {
         fprintf(stderr, 
            "json error: channel missing or invalid in entry %d\n", i);
         continue;
      }
      ssid_s = json_object_get_string(ssid);
      if (strlen(ssid_s) > SSID_MAX_LEN) {
         fprintf(stderr, 
            "json error: SSID to long in entry %d\n", i);
         continue;         
      }
      snr_n = json_object_get_int(snr);
      channel_n = json_object_get_int(channel);
      /* TODO: more validation of SSID, SNR and Channel data could
         maybe be needed. */
      if (handle_access_point(ssid_s, snr_n, channel_n, first) != 0)
         return 1;
   }

   return 0;
}

/*
 * parse_access_points_file
 *
 * Parse JSON data in access_point file
 */
int
parse_access_points_file(const char* fn, int first)
{
   int res = 0;

   json_object *root = json_object_from_file(fn);
   if (!root) {
      fprintf(stderr, "Failed to parse json file: %s\n", fn);
      return 1;
   }

   res = parse_access_points(root, first);

   json_object_put(root);

   return res;
}

/*
 * debug_print_hash
 * 
 * Debug function to print content of hash.
 */
void 
debug_print_hash(const char *msg)
{
   ap_item_t *api, *api_tmp;

   if (!fdbg)
      return;
   printf("%s\n",msg);
   HASH_ITER(api_hh, aps, api, api_tmp) {
      printf("SSID: %s, SNR: %d, channel: %d\n", 
         api->api_ap.ap_ssid, api->api_ap.ap_snr, api->api_ap.ap_channel);
   }
}

/* 
 * usage
 */
void
usage(const char *prog)
{
   fprintf(stderr, "Usage: %s [-dt] [<file>]\n", prog);
   exit(1);
}

/*
 * main
 */
int
main(int argc, char *argv[])
{
   char *fn = "access_points"; // default input file
   int res = 0, opt;
   ap_item_t *api, *api_tmp;
   watch_data_t wd = NULL;
   const char *prog = argv[0];

   /* Handle command line arguments. */
   while ((opt = getopt(argc, argv, "dt")) != -1) {
        switch (opt) {
        case 'd': fdbg = 1; break;
        case 't': ftst = 1; break;
        default:
         usage(prog);
        }
    }
   argc -= optind;
   argv += optind;

   /* Check if we have a file name. */
   if (argc == 1)
      fn = argv[0];
   else if (argc > 1)
      usage(prog);

   /* Read the file a first time */
   res = parse_access_points_file(fn, 1);
   if (res)
         return res;
   if ((wd = watch_init(fn)) ==  NULL) {
      fprintf(stderr, "Failed to watch file for changes: %s\n", fn);
      return 1;      
   }

   debug_print_hash("*** After first read:");

   /* Open named pipe for IPC */
   mkfifo(CTEA_FIFO, 0666);
   if ((fifo_fd = open(CTEA_FIFO, O_CREAT | O_WRONLY)) < 0) {
      fprintf(stderr, "Failed to open FIFO: %s\n", CTEA_FIFO);
      return 1;      

   }

   /*
    * Loop looking for changes of access_points, until interrupted
    */
   for (;;) {
      /* Check if file has changed */
      if (watch_wait(wd)) {
         fprintf(stderr, "Failed to watch file for changes: %s\n", fn);
         return 1;
      }
      /* File has changed, reparse and look for changes. */
      /* Clear the api_found flag for all previous items. */
      HASH_ITER(api_hh, aps, api, api_tmp) {
            api->api_found = 0;
      }
      /* Reparse the file and send updates */
      res = parse_access_points_file(fn, 0);
      if (res)
         return res;

      debug_print_hash("*** After new read:");
   
      /* Now look for old entries that have been removed. */
      access_point_data_t new;
      memset(&new, 0, sizeof(new));
      HASH_ITER(api_hh, aps, api, api_tmp) {
         if (!api->api_found) {
            /* AP has been removed, inform and purge it */
            inform_access_point_update(&api->api_ap, &new);
            HASH_DELETE(api_hh, aps, api);
            free(api);
         }
      }
   
      debug_print_hash("*** After purge:");
      if (ftst) {
         // When test flag is used we only do one update
         break;
      }
   }
   /* Clean up hash. */
   if (aps != NULL) {
      HASH_ITER(api_hh, aps, api, api_tmp) {
         HASH_DELETE(api_hh, aps, api);
         free(api);
      }
   }

   /* Clean up FIFO */
   close(fifo_fd);
   unlink(CTEA_FIFO);

   /* Clean up watch */
   watch_close(wd);

   return res;
}
