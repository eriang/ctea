/*
 * ctea_app_b.c
 * 
 * App B for Code Test by Erik Anggard (ctea)
 *
 * Overview:
 * - Read FIFO (named pipe) for updates
 * - Check what has changed and print it
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "ctea.h"


/*
 * Globals
 */
int fifo_fd = -1; // FIFO (named pipe) FD used for IPC App A->B
int fdbg = 0; // debug flag
int ftst = 0; // test flag

/*
 * print_access_point_update
 *
 * Check diferences between an old and a new AP data set and print
 * the difference.
 */
void
print_access_point_update(access_point_data_t *old, access_point_data_t *new)
{
   if (old->ap_ssid[0] == '\0') {
      /* This is a new entry */
      printf("%s is added to the list with SNR %d and channel %d\n",
         new->ap_ssid, new->ap_snr, new->ap_channel);
   } else if (new->ap_ssid[0] == '\0') {
      printf("%s is removed from the list\n",
         old->ap_ssid);
   } else {
      if (old->ap_snr != new->ap_snr)
         printf("%s's SNR has changed from %d to %d\n",
            old->ap_ssid, old->ap_snr, new->ap_snr);
      if (old->ap_channel != new->ap_channel)
         printf("%s's channel has changed from %d to %d\n",
            old->ap_ssid, old->ap_channel, new->ap_channel);
   }
}

/*
 * main
 *
 * Open FIFO and wait for data to process.
 */
int
main(int argc, char *argv[])
{
   access_point_data_t msg[2];
   int opt;

   /* Handle command line arguments. */
   while ((opt = getopt(argc, argv, "dt:")) != -1) {
        switch (opt) {
        case 'd': fdbg = 1; break;
        case 't': ftst = atoi(optarg); break;
        default:
            fprintf(stderr, "Usage: %s [-d][-tN]\n", argv[0]);
            return 1;
        }
    }

   /* Open FIFO (named pipe) for IPC */
   mkfifo(CTEA_FIFO, 0666);
   if ((fifo_fd = open(CTEA_FIFO, O_RDONLY)) < 0) {
      fprintf(stderr, "Failed to open FIFO: %s\n", CTEA_FIFO);
      return 1;      

   }

   /* Read FIFO until interrupted. */
   for (;;) {
      if (read(fifo_fd, msg, sizeof(msg)) == sizeof(msg)) {
         print_access_point_update(&msg[0], &msg[1]);
         if (ftst == 1) {
            // When test flag is used we only read given count of updates
            break;
         } else if (ftst > 1)
            ftst--;
      }
   }
 
   /* Clean up */
   close(fifo_fd);
   unlink(CTEA_FIFO);

   return 0;
}
