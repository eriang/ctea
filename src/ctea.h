/*
 * ctea.h
 * 
 * Common definitions for Code Test by Erik Anggard (ctea)
 */

#define SSID_MAX_LEN     32   // Max lenght of a Wifi SSID

/*
 * access_point_data_t
 *
 * Used for storing and transfering data for an access point.
 */
typedef struct access_point_data {
   char     ap_ssid[SSID_MAX_LEN+1];
   int32_t  ap_snr;
   int32_t  ap_channel;
} access_point_data_t;

// Path FIFO to (named pipe) for IPC between App A and B
#define CTEA_FIFO   "/tmp/ctea_fifo"
