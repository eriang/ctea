/*
 * ctea_watch.h
 * 
 * Watch functions for Code Test by Erik Anggard (ctea)
 *
 * Used for waiting for a file to change
 */

typedef struct watch_data_instance_t* watch_data_t;

/* Prototypes */
watch_data_t watch_init(const char *fn);
int watch_wait(watch_data_t wd);
void watch_close(watch_data_t wd);
