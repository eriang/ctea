/*
 * ctea_watch.c
 * 
 * Watch functions for Code Test by Erik Anggard (ctea)
 *
 * Used for waiting for a file to change.
 *
 * Note: there are two implementations below, one for linux 
 * using inotify and one for others (e.g. MacOS that doesn't
 * have inotify, it will simply check if the file changed
 * once per second).
 *
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#if defined(__linux__)
#include <errno.h>
#include <sys/inotify.h>
#endif

#include "ctea_watch.h"

#if defined(__linux__)
struct watch_data_instance_t {
   int wd_fd;
};

/* 
 * watch_init
 *
 * Init watch and return opaque pointer.
 */
watch_data_t
watch_init(const char *fn)
{
   int fd = inotify_init();
   if (fd == -1) {
      perror("inotify_init");
      return NULL;
   }
   if (inotify_add_watch(fd, fn, IN_CLOSE) == -1) {
      return NULL;
   }

   watch_data_t wd = malloc(sizeof(*wd));
   wd->wd_fd = fd;

   return wd;
}

/* 
 * watch_wait
 *
 * Wait for watch (ie. for file to change).
 */
int
watch_wait(watch_data_t wd)
{
   char buf[4096]
       __attribute__ ((aligned(__alignof__(struct inotify_event))));
   const struct inotify_event *event;
   ssize_t len;
   int got_one = 0;

   for (;;) {
       len = read(wd->wd_fd, buf, sizeof(buf));
       if (len == -1 && errno != EAGAIN) {
           perror("read");
           return 1;
       }
       if (len <= 0)
         continue;

       for (char *ptr = buf; ptr < buf + len;
               ptr += sizeof(struct inotify_event) + event->len) {
           event = (const struct inotify_event *) ptr;
           if (event->mask & IN_CLOSE_WRITE) {
               got_one = 1;
           }
       }

       if (got_one)
         return 0;
   }
}

/* 
 * watch_close
 *
 * Clean up.
 */
void
watch_close(watch_data_t wd)
{
   close(wd->wd_fd);
   free(wd);
}

#else

struct watch_data_instance_t {
   time_t wd_mtime;
   const char *wd_fn;
};

/*
 * get_file_mtime
 * 
 * Return modification time of given file or 0 if not found.
 */
time_t
get_file_mtime(const char *fn)
{
   struct stat st;

   if (stat(fn, &st))
      return (time_t)0;

   return st.st_mtime;
}

watch_data_t
watch_init(const char *fn)
{
   time_t mtime;

   if ((mtime = get_file_mtime(fn)) == 0)
      return NULL;

   watch_data_t wd = malloc(sizeof(*wd));
   wd->wd_mtime = mtime;
   wd->wd_fn = fn;

   return wd;
}

int
watch_wait(watch_data_t wd)
{
   time_t new_mtime;
   for (;;) {
      if ((new_mtime = get_file_mtime(wd->wd_fn)) == 0)
         return 1;
      if (new_mtime != wd->wd_mtime) {
         wd->wd_mtime = new_mtime;
         return 0;
      }
      sleep(1);
   }
}

void
watch_close(watch_data_t wd)
{
   free(wd);
}
#endif