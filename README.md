# Code Test by Erik Anggard (CTEA)

This the solution to a code test by Erik Änggård.

# Code Exercise
Environment: Linux Programming Language: C
Your task is to create two separate applications communicating with each other 
(interprocess communication) where application A monitors the changes to a file which
contains surrounding Wireless APs in JSON format and informs application B which is
responsible for displaying the change in format of:
- SSID’s SNR and/or channel value has changed from X to Y
- SSID is added to the list with SNR <SNR> and channel <CHANNEL> 
- SSID is removed from the list.

# Implementation

- App A (ctea_app_a) will read the access_points file on start up and read the AP
  data in json and store into a hash (hashed on the SSID). It will also open up a
  FIFO and wait for App B (ctea_app_b) to connect.
- It will then monitor the file for changes, when detected it will read the json
  data again, compare it to what is stored in the hash. If something differs it 
  will write the old and new entry to the FIFO, and update the hash.
- App B will read the FIFO and display the differences between the old and new
  entry (if old is empty it is a new entry, if new is empty it has been removed).

# Building the code

- Check out this repository on a Linux machine with the standard gnu gcc toolchain 
- Install json-c (eg. on debian: apt-get install libjson-c-dev)
- Install uthash (eg. on debian: apt-get install uthash-dev)
- cd src
- make

Two binaries ctea_app_a and ctea_app_b should now be available.

# Running the test

In src do:
- cp data/access_points1.json access_points
- ./ctea_app_a

In the same directory but in a new terminal do:
- ./ctea_app_b

In a third terminal do:
- cp data/access_points2.json access_points

In the second terminal you should now see:
```
MyAP's SNR has changed from 63 to 82
YourAP's channel has changed from 1 to 6
HerAP is added to the list with SNR 71 and channel 1
HisAP is removed from the list
````

# Running regression test
In src do:
- make regression_test

End of output should be:
```
Regression test: OK
```

# Running regression test using docker
Make sure you have docker installed.

In root do:
- docker build --tag ctea_reg:latest -f Dockerfile.regression .
- docker run -it ctea_reg:latest

End of output should be:
```
Regression test: OK
```

# Some notes

* Some errors should probably be handled more gracefully, not just exiting.
* The default input file is hard-coded to be "access_points", but it is possible
to specifiy another file name on the command line.
* I used FIFO (named pipe) for IPC, in a real world scenario it would probably be better 
to use message queues or some event bus technology.
* Because of the way my implementation works I don't get exactly the same order of
the output as that one given in the assignment (removed AP's will get listed last
in my implementation).
* There is a typo in the assignment, it says under Expected output:
```
MyAP’s SNR has changed from 63 to 62
```
but looking at the data it should say 'from 63 to 82'

